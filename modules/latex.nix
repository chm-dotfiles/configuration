{
  pkgs,
  inputs,
  username,
  ...
}: {
  environment.systemPackages = with pkgs; [
    texlive.combined.scheme-full
    gnuplot
  ];
}
