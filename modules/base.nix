{
  pkgs,
  inputs,
  username,
  ...
}: let
  unstable = import inputs.unstable {
    system = "x86_64-linux";
  };
in {
  # Set your time zone.
  time.timeZone = "Europe/Vienna";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_AT.UTF-8";
    LC_IDENTIFICATION = "de_AT.UTF-8";
    LC_MEASUREMENT = "de_AT.UTF-8";
    LC_MONETARY = "de_AT.UTF-8";
    LC_NAME = "de_AT.UTF-8";
    LC_NUMERIC = "de_AT.UTF-8";
    LC_PAPER = "de_AT.UTF-8";
    LC_TELEPHONE = "de_AT.UTF-8";
    LC_TIME = "de_AT.UTF-8";
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  nix.settings.experimental-features = ["nix-command" "flakes"];
  nix.settings.trusted-users = ["root" "${username}"];

  environment.systemPackages = with pkgs; [
    # Minimal packages needed for this config
    zsh
    vim
    wget
    git
    just
    home-manager
    unstable.nh

    # useful
    nix-index # "locate/grep" for files in all nix packages
    comma # temporarily installing packages in current shell
    ripgrep
    lsd
    tmux
    psmisc ## e.g., killall, fuser, pstree
    htop
    mc

    # basic system administration
    dnsutils
    mtr
    ncdu
    pmutils # power-management utils
    lsof
    usbutils
    hwinfo
    iotop
    fuse
    ddrescue
    gpart
    gparted
    hddtemp
    hdparm
    hwdata
    acpi
    rsync
    traceroute
    util-linux
    vnstat
    file
    pciutils
    ncftp

    mmv
    less
    mktemp
    pwgen
    inotify-tools

    # compression/decompression tools
    zip
    unzip
    bzip2
    gzip
    rar
    gnutar
  ];
}
