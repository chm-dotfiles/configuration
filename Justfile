

hostname := `uname --nodename`
username := `id --user --name`

default: switch home

s host=hostname:
  sudo nice time nixos-rebuild switch --flake $".#{{host}}" 

switch host=hostname:
  nice time nh os switch . -H {{host}} --ask



h user=username:
  nice time home-manager switch --flake $".#{{user}}"

home user=username:
  nice time nh home switch . --ask


up:
  nice nix flake update && \
  git add flake.lock && \
  git commit -m 'updated flake.lock via just up' && \
  echo "finished without error."

# Update specific input
# Usage: just upp nixpkgs
upp input:
  nix flake lock --update-input {{input}}

history:
  nix profile history --profile /nix/var/nix/profiles/system

repl:
  nix repl --extra-experimental-features repl-flake .#

clean:
  nh clean user --keep 5 --ask
  nh clean all --keep 5 --ask

c:
  # remove all generations older than 7 days
  sudo nix profile wipe-history --profile /nix/var/nix/profiles/system  --older-than 7d

gc:
  # garbage collect all unused nix store entries
  sudo nix store gc --debug
  sudo nix-collect-garbage --delete-old


fmt:
  # format the nix files in this repo
  nix fmt

index:
  #nix run 'nixpkgs#nix-index' --extra-experimental-features 'nix-command flakes'
  filename="index-$(uname -m | sed 's/^arm64$/aarch64/')-$(uname | tr A-Z a-z)" && \
  mkdir -p ~/.cache/nix-index && cd ~/.cache/nix-index && \
  wget -q -N https://github.com/Mic92/nix-index-database/releases/latest/download/$filename && \
  ln -f $filename files

