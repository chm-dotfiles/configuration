{
  config,
  pkgs,
  inputs,
  ...
}: let
  userSettings = import ./settings.nix;
in {
  imports = [
    (import ../../apps/cli/base.nix {inherit pkgs inputs userSettings config;})
    (import ../../apps/dev {inherit pkgs inputs userSettings config;})
  ];

  home.username = userSettings.username;
  home.homeDirectory = "/home/${userSettings.username}";
  home.stateVersion = "23.11";

  systemd.user.tmpfiles.rules = [
    "d /home/${userSettings.username}/Projects"
    "d /home/${userSettings.username}/tmp"
  ];
}
