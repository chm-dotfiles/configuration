{
  pkgs,
  inputs,
  ...
}: let
  shellAliases = {
    v = "nvim";
    vdiff = "nvim -d";
  };
in {
  xdg.configFile = {
    # base config
    "nvim" = {
      source = inputs.astronvim;
    };
    # my cusotom astronvim config, astronvim will load it after base config
    # https://github.com/AstroNvim/AstroNvim/blob/v3.32.0/lua/astronvim/bootstrap.lua#L15-L16
    "astronvim/lua/user" = {
      source = ./astronvim_user;
      force = true;
    };
  };

  home.shellAliases = shellAliases;

  programs = {
    neovim = {
      enable = true;
      defaultEditor = true;

      viAlias = true;
      vimAlias = true;

      withPython3 = true;
      withNodeJs = true;
      extraPackages = [];

      # currently we use lazy.nvim as neovim's package manager, so comment this one.
      plugins = with pkgs.vimPlugins; [
        # search all the plugins using https://search.nixos.org/packages
        luasnip
      ];
    };
  };
  home = {
    packages = with pkgs; [
      git
      #-- c/c++
      gcc # c/c++ compiler, required by nvim-treesitter!
      gnumake
      # cmake
      # cmake-language-server
      # checkmake
      # llvmPackages.clang-unwrapped # c/c++ tools with clang-tools such as clangd
      # gdb
      # lldb

      #-- python
      # nodePackages.pyright # python language server
      # python311Packages.black   # python formatter
      # python311Packages.ruff-lsp

      #-- rust
      # rust-analyzer
      # cargo  # rust package manager
      # rustfmt

      #-- nix
      nil
      # nixd
      statix # Lints and suggestions for the nix programming language
      deadnix # Find and remove unused code in .nix source files
      alejandra # Nix Code Formatter

      #-- golang
      # go
      # gomodifytags
      # iferr  # generate error handling code for go
      # impl   # generate function implementation for go
      # gotools  # contains tools like: godoc, goimports, etc.
      # gopls  # go language server
      # delve  # go debugger

      #-- lua
      stylua
      lua-language-server

      #-- bash
      # nodePackages.bash-language-server
      # shellcheck
      # shfmt

      #-- javascript/typescript --#
      # nodePackages.typescript
      # nodePackages.typescript-language-server
      # # HTML/CSS/JSON/ESLint language servers extracted from vscode
      # nodePackages.vscode-langservers-extracted
      # nodePackages."@tailwindcss/language-server"

      #-- CloudNative
      # nodePackages.dockerfile-language-server-nodejs
      # terraform
      # terraform-ls
      # jsonnet
      # jsonnet-language-server
      # hadolint  # Dockerfile linter

      #-- Others
      taplo # TOML language server / formatter / validator
      # nodePackages.yaml-language-server
      # sqlfluff    # SQL linter
      # actionlint  # GitHub Actions linter
      # buf  # protoc plugin for linting and formatting
      # proselint  # English prose linter

      #-- Misc
      tree-sitter # common language parser/highlighter
      nodePackages.prettier # common code formatter
      marksman # language server for markdown
      glow # markdown previewer

      #-- Optional Requirements:
      gdu # disk usage analyzer, required by AstroNvim
      ripgrep # fast search tool, required by AstroNvim's '<leader>fw'(<leader> is space key)
      lazygit # git ui toggle terminal (<leader>tl or <leader>gg)
      bottom # process viewer toggle terminal (<leader>tt)
    ];
  };
}
