return {
  -- Add the community repository of plugin specifications
  "AstroNvim/astrocommunity",
  -- example of importing a plugin, comment out to use it or add your own
  -- available plugins can be found at https://github.com/AstroNvim/astrocommunity
  -- { import = "astrocommunity.completion.copilot-lua-cmp" },


  -- Check https://codeberg.org/ryan4yin/nix-config/src/branch/main/home/base/desktop/neovim/astronvim_user/init.lua

  { import = "astrocommunity.colorscheme.catppuccin" },
  --  Neovim's answer to the mouse 🦘
  { import = "astrocommunity.motion.leap-nvim" },
  -- Enhanced f/t motions for Leap
  { import = "astrocommunity.motion.flit-nvim" },
  -- Extensible Neovim Scrollbar 
  { import = "astrocommunity.scrolling.nvim-scrollbar" },
  -- 🧶 Automatically save your changes in NeoVim 
  { import = "astrocommunity.editing-support.auto-save-nvim" },
  -- ✅ Highlight, list and search todo comments in your projects 
  { import = "astrocommunity.editing-support.todo-comments-nvim" },
  -- Language Support
  ---- Frontend & NodeJS
  -- { import = "astrocommunity.pack.html-css" },
  -- -- { import = "astrocommunity.pack.tailwindcss" },
  -- ---- Configuration Language
  -- { import = "astrocommunity.pack.markdown" },
  -- { import = "astrocommunity.pack.json" },
  -- { import = "astrocommunity.pack.yaml" },
  -- { import = "astrocommunity.pack.toml" },
  -- ---- Backend
  -- { import = "astrocommunity.pack.lua" },
  -- { import = "astrocommunity.pack.nix" },  -- manually add config for nix, comment this one.
  -- { import = "astrocommunity.pack.proto" },
  -- ---- Operation & Cloud Native
  -- { import = "astrocommunity.pack.bash" },
  -- { import = "astrocommunity.pack.cmake" },
  -- { import = "astrocommunity.pack.docker" },
  -- -- AI Assistant
  -- -- { import = "astrocommunity.completion.copilot-lua-cmp" },
  -- -- Custom copilot-lua to enable filtypes: markdown
  -- -- {
  -- --   "zbirenbaum/copilot.lua",
  -- --   opts = function(_, opts)
  -- --     opts.filetypes = {
  -- --       yaml = true;
  -- --       markdown = true,
  -- --     }
  -- --   end,
  -- -- },
  {
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
      {"nvim-lua/plenary.nvim"},
      {"nvim-treesitter/nvim-treesitter"}
    }
  },
  -- Language Parser for syntax highlighting / indentation / folding / Incremental selection
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      local utils = require "astronvim.utils";
      opts.indent.enable = false;
      opts.ensure_installed = utils.list_insert_unique(opts.ensure_installed, {
        -- neovim
        "vim",
        "lua",
        -- operation & cloud native
        "dockerfile",
        -- "hcl",
        -- "jsonnet",
        -- "regex",
        -- "terraform",
        "nix",
      })
    end,
  },
  -- Install lsp, formmatter and others via home manager instead of Mason.nvim
  -- LSP installations
  {
    "williamboman/mason-lspconfig.nvim",
    -- overwrite ensure_installed to install lsp via home manager(except emmet_ls)
    opts = function(_, opts)
      opts.ensure_installed = {
        "emmet_ls", -- not exist in nixpkgs, so install it via mason
      }
    end,
  },
  -- Formatters/Linter installation
  {
    "jay-babu/mason-null-ls.nvim",
    -- ensure_installed nothing
    opts = function(_, opts)
      opts.ensure_installed = nil
      opts.automatic_installation = false
    end,
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    opts = function(_, opts)
      local null_ls = require "null-ls"
      local code_actions = null_ls.builtins.code_actions
      local diagnostics = null_ls.builtins.diagnostics
      local formatting = null_ls.builtins.formatting
      local hover = null_ls.builtins.hover
      local completion = null_ls.builtins.completion
      -- https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md
      if type(opts.sources) == "table" then
        vim.list_extend(opts.sources, {
          -- Common Code Actions
          code_actions.gitsigns,
          -- common refactoring actions based off the Refactoring book by Martin Fowler
          -- code_actions.refactoring,
          -- code_actions.gomodifytags,  -- Go - modify struct field tags
          -- code_actions.impl,        -- Go - generate interface method stubs
          -- code_actions.shellcheck,
          -- code_actions.proselint,   -- English prose linter
          code_actions.statix,      -- Lints and suggestions for Nix.

          -- Completion
          completion.luasnip,

          -- Diagnostic
          -- diagnostics.actionlint,  -- GitHub Actions workflow syntax checking
          -- diagnostics.buf,         -- check text in current buffer
          -- diagnostics.checkmake,   -- check Makefiles
          diagnostics.deadnix,     -- Scan Nix files for dead code.

          -- Formatting
          -- formatting.prettier, -- js/ts/vue/css/html/json/... formatter
          -- diagnostics.hadolint, -- Dockerfile linter
          -- formatting.black,     -- Python formatter
          -- formatting.ruff,      -- extremely fast Python linter
          -- formatting.goimports,  -- Go formatter
          -- formatting.shfmt,     -- Shell formatter
          -- formatting.rustfmt,   -- Rust formatter
          -- formatting.taplo,   -- TOML formatteautoindentr
          -- formatting.terraform_fmt, -- Terraform formatter
          formatting.stylua,    -- Lua formatter
          formatting.alejandra, -- Nix formatter
          formatting.sqlfluff.with({  -- SQL formatter
          extra_args = { "--dialect", "postgres" }, -- change to your dialect
        }),
        -- formatting.nginx_beautifier,  -- Nginx formatter
      })
    end
  end,
},
  --   -- Debugger installation 
  --   {
  --     "jay-babu/mason-nvim-dap.nvim",
  --     -- overrides `require("mason-nvim-dap").setup(...)`
  --     opts = function(_, opts)
  --       opts.ensure_installed = nil
  --       opts.automatic_installation = false
  --     end,
  --   },
  --
  -- -- Configure require("lazy").setup() options
  -- lazy = {
  --   defaults = { lazy = true },
  --   performance = {
  --     rtp = {
  --       -- customize default disabled vim plugins
  --       disabled_plugins = { };
  --     },
  --   },
  -- },
  --
  lsp = {
    config = {
      -- the offset_enconding of clangd will confilicts whit null-ls
      -- so we need to manually set it to utf-8
      clangd = {
        capabilities = {
          offsetEncoding = "utf-8",
        },
      },
    },
    -- enable servers that installed by home-manager instead of mason
    servers = {
  --     ---- Frontend & NodeJS
  --     -- "tsserver",     -- typescript/javascript language server
  --     "tailwindcss",  -- tailwindcss language server
  --     "html",         -- html language server
  --     "cssls",        -- css language server
  --     -- "prismals",     -- prisma language server
  --     -- "volar",        -- vue language server
  --     ---- Configuration Language
  --     "marksman",     -- markdown ls
  --     "jsonls",      -- json language server
  --     "yamlls",       -- yaml language server
  --     "taplo",         -- toml language server
  --     ---- Backend
      "lua_ls",         -- lua
  --     "gopls",          -- go
  --     "rust_analyzer",  -- rust
  --     "pyright",        -- python
  --     "ruff_lsp",       -- extremely fast Python linter and code transformation
  --     -- "jdtls",          -- java
      "nil_ls",         -- nix language server
  --     -- "bufls",          -- protocol buffer language server
  --     -- "zls",            -- zig language server
  --     ---- Operation & Cloud Nativautoindente
  --     "bashls",       -- bash
  --     "cmake",        -- cmake language server
  --     "clangd",       -- c/c++
  --     "dockerls",     -- dockerfile
  --     "jsonnet_ls",   -- jsonnet language server
  --     "terraformls",  -- terraform hcl
    },
    formatting = {
      disabled = {},
      format_on_save = {
        enabled = true,
        allow_filetypes = {
          "lua",
        },
      },
    },
  }
}
