{
  pkgs,
  userSettings,
  ...
}: {
  programs.zsh = {
    enable = true;
    prezto = {
      enable = true;
      pmodules = [
        "archive"
        "docker"
        "environment"
        "git"
        "terminal"
        "editor"
        "history"
        "directory"
        "spectrum"
        "utility"
        "completion"
        "command-not-found"
        "syntax-highlighting"
        "history-substring-search"
        "autosuggestions"
        "prompt"
        "ssh"
      ];
    };
    autocd = true;
    sessionVariables = {
      EDITOR = "nvim";
      PATH = "$PATH:$HOME/bin";
    };
    shellAliases = {
      "ls" = "lsd";
      "ll" = "lsd -la";
      "open" = "xdg-open";
      "y" = "yazi";
      "cat" = "bat";
      "cd" = "z";
    };
  };
  programs.starship = {
    enable = true;
    enableZshIntegration = true;

    #settings = pkgs.lib.importTOML ../../dots/starship.toml;
  };

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
  };
}
