{
  pkgs,
  config,
  ...
}: {
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    terminal = "tmux-256color";
    historyLimit = 100000;

    sensibleOnTop = true;

    plugins = with pkgs.tmuxPlugins; [
      better-mouse-mode
      sensible
      {
        plugin = tmux-thumbs;
        extraConfig = ''
          set -g @thumbs-unique enabled
        '';
      }
      {
        plugin = catppuccin;
        extraConfig = ''
          set -g @catppuccin_flavour 'frappe'
          set -g @catppuccin_window_status_enable "yes"
          set -g @catppuccin_window_status_icon_enable "yes"

          set -g @catppuccin_window_left_separator ""
          set -g @catppuccin_window_right_separator " "
          set -g @catppuccin_window_middle_separator "█ "
          set -g @catppuccin_window_number_position "left"

          set -g @catppuccin_window_default_fill "number"
          set -g @catppuccin_window_default_text "#W"

          set -g @catppuccin_window_current_fill "number"
          set -g @catppuccin_window_current_text "#W"

          set -g @catppuccin_status_left_separator  " "
          set -g @catppuccin_status_right_separator ""
          set -g @catppuccin_status_right_separator_inverse "no"
          set -g @catppuccin_status_fill "all"
          set -g @catppuccin_status_connect_separator "yes"

          set -g @catppuccin_status_modules "host session date_time"

          set -g @catppuccin_date_time_text "%d.%m.%Y %H:%M"

          set -g @catppuccin_directory_text "#{pane_current_path}"
        '';
      }
    ];

    extraConfig = ''
      # https://github.com/microsoft/WSL/issues/5931
      set -s escape-time 1

      bind C-j display-popup -E "tms switch"
      bind C-o display-popup -E "tms"
      bind C-x run-shell "smug stop"
    '';
  };

  home.packages = with pkgs; [
    tmux-sessionizer
    smug
  ];

  programs.zsh.shellAliases = {"s" = "smug";};
  home.file."${config.home.homeDirectory}/.config/smug" = {
    source = ./../../dots/smug;
  };
}
