{
  pkgs,
  inputs,
  userSettings,
  config,
  ...
}: {
  imports = [
    (import ./zsh.nix {inherit pkgs inputs userSettings;})
    (import ./neovim {inherit pkgs inputs userSettings;})
    (import ./tmux.nix {inherit pkgs inputs userSettings config;})
    (import ./git.nix {inherit pkgs inputs userSettings config;})
  ];

  home.packages = with pkgs; [
    neofetch
  ];

  programs.yazi = {
    enable = true;
    theme = builtins.fromTOML (builtins.readFile ./yazi/theme-catppuccin-mocha.toml);
  };

  programs.bat = {
    enable = true;
    config = {
      theme = "Coldark-Dark";
    };
  };
}
