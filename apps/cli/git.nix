{
  pkgs,
  inputs,
  userSettings,
  config,
  ...
}: {
  programs.git = {
    enable = true;
    userName = "${userSettings.name}";
    userEmail = "${userSettings.email}";

    diff-so-fancy.enable = true;

    aliases = {
      staash = "stash --all";
      bb = "!better-git-branch.sh";
    };
    includes = [
      {
        condition = "gitdir/i:~/Projects/htl/";
        contents.user.email = "${userSettings.workEmail}";
      }
    ];
  };

  home.file."${config.home.homeDirectory}/bin/better-git-branch.sh" = {
    source = ./../../dots/bin/better-git-branch.sh;
  };
}
