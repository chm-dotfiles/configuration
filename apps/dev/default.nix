{
  pkgs,
  inputs,
  ...
}: {
  home.packages = [
    inputs.devenv.packages."${pkgs.system}".devenv
    pkgs.cachix
    pkgs.jq
    pkgs.gitui
  ];

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
}
