{
  inputs = {
    # NixPkgs (nixos-23.11)
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";

    # NixPkgs Unstable (nixos-unstable)
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    # Home Manager (release-23.11)
    home-manager.url = "github:nix-community/home-manager/release-24.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # NixOS profiles to optimize settings for different hardware.
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    # NixOS on WSL
    nixos-wsl.url = "github:nix-community/NixOS-WSL";
    nixos-wsl.inputs.nixpkgs.follows = "nixpkgs";

    # Fast, Declarative, Reproducible, and Composable Developer Environments
    devenv.url = "github:cachix/devenv/v1.3.1";
    devenv.inputs.nixpkgs.follows = "nixpkgs";

    # The Uncompromising Nix Code Formatter
    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    alejandra.inputs.nixpkgs.follows = "nixpkgs";

    # add git hooks to format nix code before commit
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    pre-commit-hooks.inputs.nixpkgs.follows = "nixpkgs";

    # AstroNvim is an aesthetic and feature-rich neovim config.
    astronvim.url = "github:AstroNvim/AstroNvim/v3.45.3";
    astronvim.flake = false;

    # Visual Studio Code Server support in NixOS
    #vscode-server.url = "github:nix-community/nixos-vscode-server";
  };

  nixConfig = {
    extra-trusted-public-keys = "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=";
    extra-substituters = "https://devenv.cachix.org";
  };

  outputs = inputs: let
    system = "x86_64-linux";
    hosts = builtins.attrNames (builtins.readDir ./hosts);
    pkgs = inputs.nixpkgs.legacyPackages.${system};

    checks.${system} = {
      pre-commit-check = inputs.pre-commit-hooks.lib.${system}.run {
        src = ./.;
        hooks = {
          alejandra.enable = true; # formatter
          deadnix.enable = false; # detect unused variable bindings in `*.nix`
          statix.enable = false; # lints and suggestions for Nix code(auto suggestions)
        };
      };
    };
  in {
    # for every entry in "hosts" directory, built nixosConfiguration
    nixosConfigurations = inputs.nixpkgs.lib.attrsets.genAttrs hosts (
      name:
        inputs.nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [./hosts/${name}];
          specialArgs = {inherit inputs;};
        }
    );

    homeConfigurations = {
      chris = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = inputs.nixpkgs.legacyPackages.${system};
        modules = [./users/chris];
        extraSpecialArgs = {inherit inputs;};
      };
      mac = inputs.home-manager.lib.homeManagerConfiguration {
        pkgs = inputs.nixpkgs.legacyPackages.${system};
        modules = [./users/mac];
        extraSpecialArgs = {inherit inputs;};
      };
    };

    formatter.${system} = inputs.nixpkgs.legacyPackages.${system}.alejandra;

    devShells.${system}.default = pkgs.mkShellNoCC {
      packages = with pkgs; [git];
      shellHook = ''${checks.${system}.pre-commit-check.shellHook}'';
    };
    packages.${system}.default = pkgs.git;
  };
}
